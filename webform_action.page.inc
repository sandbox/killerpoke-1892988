<?php

include_once 'webform_action.api.php';

function webform_action_admin_page_form($form, &$form_state, $node) {

  $settings = webform_action_get_settings($node->nid);

  $action_default = 'http://example.com';
  $enable_default = 0;

  if(isset($settings['action'])) {
    $action_default = $settings['action'];
  }

  if(isset($settings['enable'])) {
    $enable_default = $settings['enable'];
  }

  if(isset($settings['plain'])) {
    $plain_default = $settings['plain'];
  }

  $form['#submit'] = array(
    'webform_action_admin_page_form_submit',
  );

  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
  );

  $form['enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable'),
    '#description' => t('Enable Webform action, and do not let webform process subscriptions.'),
    '#default_value' => $enable_default,
  );

  $form['plain'] = array(
    '#type' => 'checkbox',
    '#title' => t('Plain keys'),
    '#description' => t('Use plain keys (remove submitted[] container).'),
    '#default_value' => $plain_default,
  );

  $form['action'] = array(
    '#type' => 'textfield',
    '#title' => t('Action URL'),
    '#description' => t('The URL endpoint, you want to POST the form data.'),
    '#default_value' => $action_default,
    '#maxlength' => 255,
    '#required' => true,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

function webform_action_admin_page_form_submit($form, &$form_state) {

  $settings = unserialize(variable_get('webform_action_settings'));
  if(!is_array($settings)) {
    $settings = array();
  }

  $settings[$form_state['values']['nid']] = array(
    'action' => $form_state['values']['action'],
    'enable' => $form_state['values']['enable'],
    'plain' => $form_state['values']['plain'],
  );
  variable_set('webform_action_settings', serialize($settings));

  drupal_set_message(t('The form action settings have been updated.'));
}

