<?php

function webform_action_get_settings($nid) {
  $settings = unserialize(variable_get('webform_action_settings'));
  if(!is_array($settings) || !isset($settings[$nid]) || !is_array($settings[$nid])) {
    return array();
  }
  return $settings[$nid];
}

function webform_action_set_settings($nid, $get_settings) {
  $settings = unserialize(variable_get('webform_action_settings'));
  if(!is_array($settings)) {
    $settings = array();
  }

  $settings[$nid] = $get_settings;
  variable_set('webform_action_settings', serialize($settings));
}